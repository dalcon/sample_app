require 'spec_helper'

describe "Páginas Usuarios" do

  subject { page }

  describe "index" do
    let(:usuario) { FactoryGirl.create(:usuario) }

    before do
      ingresar usuario
      visit usuarios_path
    end

    it { should have_title("Todos los usuarios") }
    it { should have_content("Todos los usuarios") }

    describe "paginación" do

      before(:all) { 30.times { FactoryGirl.create(:usuario) } }
      after(:all) { Usuario.delete_all }

      it { should have_selector('div.pagination') }

      it "debería listar cada usuario" do
        Usuario.paginate(page: 1).each do |usuario|
          expect(page).to have_selector( 'li', text: usuario.nombre )
        end
      end
    end

    describe "links eliminar" do

      it { should_not have_link('eliminar') }

      describe "como un usuario admin" do
        let(:admin) { FactoryGirl.create(:admin) }
        before do
          ingresar admin
          visit usuarios_path
        end

        it { should have_link('eliminar', href: usuario_path(Usuario.first)) }
        it "debería poder eliminar otro usuario" do
          expect do
            click_link('eliminar', match: :first)
          end.to change(Usuario, :count).by(-1)
        end
        it { should_not have_link('eliminar', href: usuario_path(admin)) }
      end
    end
  end

  describe "Página perfil" do
    let(:usuario) { FactoryGirl.create(:usuario) }
    let!(:m1) { FactoryGirl.create(:micropost, usuario: usuario, content: "Foo") }
    let!(:m2) { FactoryGirl.create(:micropost, usuario: usuario, content: "Bar") }

    before { visit usuario_path(usuario) }

    it { should have_content(usuario.nombre) }
    it { should have_title(usuario.nombre) }

    describe "microposts" do
      it { should have_content(m1.content) }
      it { should have_content(m2.content) }
      it { should have_content(usuario.microposts.count) }
    end

    describe "botones seguir/noseguir" do
      let(:otro_usuario) { FactoryGirl.create(:usuario) }
      before { ingresar usuario }

      describe "seguiendo un usuario" do
        before { visit usuario_path(otro_usuario) }

        it "debería incrementarse el contador de usuarios seguidos" do
          expect do
            click_button "Seguir"
          end.to change(usuario.usuarios_seguidos, :count).by(1)
        end

        it "debería incremetarse el contador de contador de seguidores del otro usuario" do
          expect do
            click_button "Seguir"
          end.to change(otro_usuario.seguidores, :count).by(1)
        end

        describe "commutando el botón" do
          before { click_button "Seguir" }
          it { should have_xpath("//input[@value='No seguir']") }
        end
      end

      describe "no seguir un usuario" do
        before do
          usuario.seguir!(otro_usuario)
          visit usuario_path(otro_usuario)
        end

        it "debería decrementarse el contador de usuarios seguidos" do
          expect do
            click_button "No seguir"
          end.to change(otro_usuario.seguidores, :count).by(-1)
        end

        describe "commutando el botón" do
          before { click_button "No seguir" }
          it { should have_xpath("//input[@value='Seguir']") }
        end
      end
    end
  end

  describe "Página para inscribirse" do
    before { visit inscribirse_path }

    it { should have_content('Inscribirse') }
    it { should have_title(full_title('Inscribirse')) }
  end

  describe "inscribirse" do

    before { visit inscribirse_path }

    let(:suministrar) { "Crear mi cuenta" }

    describe "con información inválida" do
      it "no debería crear un usuario" do
        expect { click_button suministrar }.not_to change(Usuario, :count)
      end
    end

    describe "con información válida" do
      before do
        fill_in "Nombre", with: "Usuario Ejemplo"
        fill_in "Email", with: "usuario@ejemplo.com"
        fill_in "Contraseña", with: "foobar"
        fill_in "Confirmación", with: "foobar"
      end

      it "debería crear un usuario" do
        expect { click_button suministrar }.to change(Usuario, :count).by(1)
      end

      describe "despues de guardar un usuario" do
        before { click_button suministrar }
        let(:usuario) { Usuario.find_by(email: "usuario@ejemplo.com") }

        it { should have_link('Salir') }
        it { should have_title(usuario.nombre) }
        it { should have_selector('div.alert.alert-success', text: "Bienvenido") }
      end
    end
  end

  describe "editar" do
    let(:usuario) { FactoryGirl.create(:usuario) }
    before do
      ingresar usuario
      visit edit_usuario_path(usuario)
    end

    describe "página" do
      it { should have_content("Actualiza tu perfil") }
      it { should have_title("Editar Usuario") }
      it { should have_link("cambiar", href: "http://gravatar.com/emails") }
    end

    describe "con información inválida" do
      before { click_button "Guardar cambios" }

      it { should have_content('error') }
    end

    describe "con información válida" do
      let(:nuevo_nombre) { "Nuevo Nombre" }
      let(:nuevo_email)  { "new@example.com" }

      before do
        fill_in "Nombre",               with: nuevo_nombre
        fill_in "Email",                with: nuevo_email
        fill_in "Contraseña",           with: usuario.password
        fill_in "Confirmar contraseña", with: usuario.password
        click_button "Guardar cambios"
      end

      it { should have_title(nuevo_nombre) }
      it { should have_selector('div.alert.alert-success') }
      it { should have_link("Salir", href: salir_path) }
      specify { expect(usuario.reload.nombre).to eq nuevo_nombre }
      specify { expect(usuario.reload.email).to  eq nuevo_email }
    end

    describe "atributos olvidades" do
      let(:params) do
        { usuario: { admin: true, password: usuario.password,
                     password_confirmation: usuario.password } }
      end

      before do
        ingresar usuario, no_capybara: true
        patch usuario_path(usuario), params
      end
      specify { expect(usuario.reload).not_to be_admin }
    end
  end

  describe "siguiendo/seguidores" do
    let(:usuario) { FactoryGirl.create(:usuario) }
    let(:otro_usuario) { FactoryGirl.create(:usuario) }
    before { usuario.seguir!(otro_usuario) }

    describe "usuarios seguidos" do
      before do
        ingresar usuario
        visit siguiendo_usuario_path(usuario)
      end

      it { should have_title(full_title('Siguiendo')) }
      it { should have_selector('h3', text: 'Siguiendo') }
      it { should have_link(otro_usuario.nombre, href: usuario_path(otro_usuario)) }
    end

    describe "seguidores" do
      before do
        ingresar otro_usuario
        visit seguidores_usuario_path(otro_usuario)
      end

      it { should have_title(full_title('Seguidores')) }
      it { should have_selector('h3', text: 'Seguidores') }
      it { should have_link(usuario.nombre, href: usuario_path(usuario)) }
    end
  end
end
