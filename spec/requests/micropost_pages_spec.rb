require 'spec_helper'

describe "Páginas de Microposts" do

  subject { page }

  let(:usuario) { FactoryGirl.create(:usuario) }
  before { ingresar usuario }

  describe "creación de micropost" do
    before { visit root_path }

    describe "con información inválida" do

      it "no debería crear un micropost" do
        expect { click_button "Postear" }.not_to change(Micropost, :count)
      end

      describe "mensajes de error" do
        before { click_button "Postear" }
        it { should have_content('error') }
      end
    end

    describe "con información válida" do
      before { fill_in 'micropost_content', with: "Lorem ipsum" }

      it "debería crear un micropost" do
        expect { click_button "Postear" }.to change(Micropost, :count).by(1)
      end
    end
  end

  describe "borrado de micropost" do
    before { FactoryGirl.create(:micropost, usuario: usuario) }

    describe "como usuario correcto" do
      before { visit root_path }

      it "debería borrar un micropodt" do
        expect { click_link "borrar" }.to change(Micropost, :count).by(-1)
      end
    end
  end
end
