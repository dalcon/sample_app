require 'spec_helper'

describe "Autentificación" do

  subject { page }

  describe "página de ingreso" do
    before { visit ingresar_path }

    it { should have_content('Ingresar') }
    it { should have_title('Ingresar') }
  end

  describe "ingresar" do
    before { visit ingresar_path }

    describe "con información inválida" do
      before { click_button "Ingresar" }

      it { should have_title('Ingresar') }
      it { should have_selector('div.alert.alert-error') }

      describe "despues visitar otra página" do
        before { click_link "Home" }

        it { should_not have_selector('div.alert.alert-error') }
      end
    end

    describe "con información válida" do
      let(:usuario) { FactoryGirl.create(:usuario) }
      before { ingresar usuario }

      it { should have_title(usuario.nombre) }
      it { should have_link('Usuarios',      href: usuarios_path) }
      it { should have_link('Perfil',        href: usuario_path(usuario)) }
      it { should have_link('Configuración', href: edit_usuario_path(usuario)) }
      it { should have_link('Salir',         href: salir_path) }
      it { should_not have_link('Ingresar',  href: ingresar_path) }

      describe "seguido por salir" do
        before { click_link "Salir" }
        it { should have_link('Ingresar') }
      end
    end
  end

  describe "autorización" do

    describe "para usuarios no identificados" do
      let(:usuario) { FactoryGirl.create(:usuario) }

      describe "cuando intenta visitar una página protegida" do
        before do
          visit edit_usuario_path(usuario)
          fill_in "Email", with: usuario.email
          fill_in "Contraseña", with: usuario.password
          click_button "Ingresar"
        end

        describe "despues de ingresar" do

          it "debería construir la página protegida deseada" do
            expect(page).to have_title("Editar Usuario")
          end
        end
      end

      describe "en el controlador de Microposts" do

        describe "enviando a una acción create" do
          before { post microposts_path }
          specify { expect(response).to redirect_to(ingresar_path) }
        end

        describe "enviando a una acción destroy" do
          before { delete micropost_path(FactoryGirl.create(:micropost)) }
          specify { expect(response).to redirect_to(ingresar_path) }
        end
      end

      describe "en el controlador Usuarios" do

        describe "visitando la página editar" do
          before { visit edit_usuario_path(usuario) }
          it { should have_title("Ingresar") }
        end

        describe "enviar a la acción update" do
          before { patch usuario_path(usuario) }
          specify { expect(response).to redirect_to(ingresar_path) }
        end

        describe "visitando el index de usuario" do
          before { visit usuarios_path }
          it { should have_title('Ingresar') }
        end

        describe "visitando la página de usuarios a quienes sigue" do
          before { visit siguiendo_usuario_path(usuario) }
          it { should have_title('Ingresar') }
        end

        describe "visitando la página de seguidores" do
          before { visit seguidores_usuario_path(usuario) }
          it { should have_title('Ingresar') }
        end
      end

      describe "en el controlador Relacions" do
        describe "invocando a la acción create" do
          before { post relacions_path }
          specify { expect(response).to redirect_to(ingresar_path) }
        end

        describe "invocando a la acción destroy" do
          before { delete relacion_path(1) }
          specify { expect(response).to redirect_to(ingresar_path) }
        end
      end
    end

    describe "como usuario incorrecto" do
      let(:usuario) { FactoryGirl.create(:usuario) }
      let(:usuario_incorrecto) { FactoryGirl.create(:usuario, email:"incorrecto@example.com") }
      before { ingresar usuario, no_capybara: true }

      describe "enviar un requerimiento GEt a la acción Usuarios#edit" do
        before { get edit_usuario_path(usuario_incorrecto) }
        specify { expect(response.body).not_to match(full_title('Editar usuario')) }
        specify { expect(response).to redirect_to(root_url) }
      end

      describe "enviar un requerimiento PATH a la acción Usuarios#update" do
        before { patch usuario_path(usuario_incorrecto) }
        specify { expect(response).to redirect_to(root_url) }
      end
    end

    describe "como usuario no admin" do
      let(:usuario) { FactoryGirl.create(:usuario) }
      let(:no_admin) { FactoryGirl.create(:usuario) }

      before { ingresar no_admin, no_capybara: true }

      describe "enviar un requerimiento DELETE a ña acción Usuarios#destroy" do
        before { delete usuario_path(usuario) }
        specify { expect(response).to redirect_to(root_url) }
      end
    end
  end
end
