require 'spec_helper'

describe "Páginas Estáticas" do

  subject {page}

  shared_examples_for "todas las paginas estaticas" do
    it { should have_selector('h1', text: encabezado) }
    it { should have_title(full_title(titulo_pagina)) }
  end

  describe "Página Home" do
    before { visit root_path }
    let(:encabezado) { 'Sample App' }
    let(:titulo_pagina) { '' }

    it_should_behave_like "todas las paginas estaticas"
    it { should_not have_title("| Home") }

    describe "para usuarios identificados" do
      let(:usuario) { FactoryGirl.create(:usuario) }
      before do
        FactoryGirl.create(:micropost, usuario: usuario, content: "Lorem ipsum")
        FactoryGirl.create(:micropost, usuario: usuario, content: "Dolor sit amet")
        ingresar usuario
        visit root_path
      end

      it "deberia construir el feed del usuario" do
        usuario.feed.each do |item|
          expect(page).to have_selector("li##{item.id}", text: item.content)
        end
      end

      describe "contadoresd e seguidores/siguiendo" do
        let(:otro_usuario) { FactoryGirl.create(:usuario) }
        before do
          otro_usuario.seguir!(usuario)
          visit root_path
        end

        it { should have_link("0 siguiendo", href: siguiendo_usuario_path(usuario)) }
        it { should have_link("1 seguidores", href: seguidores_usuario_path(usuario)) }
      end
    end
  end

  describe "Página Help" do
    before { visit ayuda_path }
    let(:encabezado) { 'Ayuda' }
    let(:titulo_pagina) { 'Ayuda' }

    it_should_behave_like "todas las paginas estaticas"
  end

  describe "Página Acerca de" do
    before { visit acercade_path }
    let(:encabezado) { 'Acerca de Nosotros' }
    let(:titulo_pagina) { 'Acerca de Nosotros' }

    it_should_behave_like "todas las paginas estaticas"
  end

  describe "Página Contacto" do
    before { visit contacto_path }
    let(:encabezado) { 'Contacto' }
    let(:titulo_pagina) { 'Contacto' }

    it_should_behave_like "todas las paginas estaticas"
  end

  it "debería tener los links correctos en el layout" do
    visit root_path
    click_link "Acerca de"
    expect(page).to have_title(full_title('Acerca de Nosotros'))
    click_link "Ayuda"
    expect(page).to have_title(full_title('Ayuda'))
    click_link "Contacto"
    expect(page).to have_title(full_title('Contacto'))
    click_link "Home"
    expect(page).to have_title(full_title(''))
    click_link "Inscribete ahora!"
    expect(page).to have_title(full_title('Inscribirse'))
    click_link "sample app"
    expect(page).to have_title(full_title(''))
  end
end
