#def full_title(page_title)
#  base_title = "Tutorial Ruby on Rails Sample App"
#  if page_title.empty?
#    base_title
#  else
#    "#{base_title} | #{page_title}"
#  end
#end
include ApplicationHelper

def ingresar( usuario, options={})
  if options[:no_capybara]
    #ingresar cuando no esta usando capybara.
    remember_token = Usuario.nuevo_remember_token
    cookies[:remember_token] = remember_token
    usuario.update_attribute(:remember_token, Usuario.digest(remember_token))
  else
    visit ingresar_path
    fill_in "Email",      with: usuario.email
    fill_in "Contraseña", with: usuario.password
    click_button "Ingresar"
  end
end
