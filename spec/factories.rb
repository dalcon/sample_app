FactoryGirl.define do
  factory :usuario do
    sequence(:nombre) { |n| "Persona #{n}" }
    sequence(:email) { |n| "persona_#{n}@ejemplo.com" }
    password "foobar"
    password_confirmation "foobar"

    factory :admin do
      admin true
    end
  end

  factory :micropost do
    content "Lorem ipsum"
    usuario
  end
end
