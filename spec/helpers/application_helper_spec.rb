require 'spec_helper'

describe ApplicationHelper do

  describe "full_title" do
    it "deberia incluir el título de la página" do
      expect(full_title("foo")).to match(/foo/)
    end

    it "debería incluir el título base" do
      expect(full_title("foo")).to match(/^Tutorial Ruby on Rails Sample App/)
    end

    it "no debería incluir una barra para el home page" do
      expect(full_title("")).not_to match(/\|/)
    end
  end
end
