require 'spec_helper'

describe Usuario do

  before { @usuario = Usuario.new(nombre: "Usuario Ejemplo", email: "usuario@vallenova.pe",
                                  password: "foobar", password_confirmation: "foobar") }

  subject { @usuario }

  it { should respond_to(:nombre) }
  it { should respond_to(:email) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:remember_token) }
  it { should respond_to(:authenticate) }
  it { should respond_to(:admin) }
  it { should respond_to(:microposts) }
  it { should respond_to(:feed) }
  it { should respond_to(:relacions) }
  it { should respond_to(:usuarios_seguidos) }
  it { should respond_to(:relacions_reversa) }
  it { should respond_to(:seguidores) }
  it { should respond_to(:siguiendo?) }
  it { should respond_to(:seguir!) }

  it { should be_valid }
  it { should_not be_admin }

  describe "con atributo admin fijado a 'true'" do
    before do
      @usuario.save!
      @usuario.toggle!(:admin)
    end

    it { should be_admin }
  end

  describe "cuando el nombre no esta presente" do
    before { @usuario.nombre = " "}
    it { should_not be_valid }
  end

  describe "cuando el email no esta presente" do
    before { @usuario.email = " " }
    it { should_not be_valid }
  end

  describe "cuando el nombre es muy largo" do
    before { @usuario.nombre = "a" * 51 }
    it { should_not be_valid }
  end

  describe "cuando el formato del email es invalido" do
    it "debería ser inválido" do
      direcciones = %w[user@foo,com user_at_foo.org example.user@foo.
                       foo@bar_baz.com foo@bar+baz.com]
      direcciones.each do |direccion_invalida|
        @usuario.email = direccion_invalida
        expect(@usuario).not_to be_valid
      end
    end
  end

  describe "cuando el formato de email es valido" do
    it "debería se válido" do
      direcciones = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      direcciones.each do |direccion_valida|
        @usuario.email = direccion_valida
        expect(@usuario).to be_valid
      end
    end
  end

  describe "cuando la dirección de email ya fue usada" do
    before do
      usuario_con_igual_email = @usuario.dup
      usuario_con_igual_email.email = @usuario.email.upcase
      usuario_con_igual_email.save
    end

    it { should_not be_valid }
  end

  describe "cuando el password no esta presente" do
    before do
      @usuario = Usuario.new(nombre: "Usuario Ejemplo", email: "usuario@vallenova.pe",
                             password: " ", password_confirmation: " ")
    end

    it { should_not be_valid }
  end

  describe "cuando el password no esta relacionado a la confirmación" do
    before { @usuario.password_confirmation = "mismatch" }

    it { should_not be_valid }
  end

  describe "con un password corto" do
    before { @usuario.password = @usuario.password_confirmation = "a" * 5 }

    it { should be_invalid }
  end

  describe "valor de retorno del método autenticar" do
    before { @usuario.save }
    let(:usuario_encontrado) { Usuario.find_by( email: @usuario.email ) }

    describe "con password válido" do
      it { should eq usuario_encontrado.authenticate( @usuario.password ) }
    end

    describe "con password inválido" do
      let(:usuario_para_password_invalido) { usuario_encontrado.authenticate("invalido") }

      it { should_not eq usuario_para_password_invalido }
      specify { expect(usuario_para_password_invalido).to be_false }
    end
  end

  describe "remember token" do
    before { @usuario.save }
    its(:remember_token) { should_not be_blank }
  end

  describe "asociaciones de microposts" do

    before { @usuario.save }

    let!(:micropost_antiguo) do
      FactoryGirl.create(:micropost, usuario: @usuario, created_at: 1.day.ago)
    end
    let!(:micropost_mas_nuevo) do
      FactoryGirl.create(:micropost, usuario: @usuario, created_at: 1.hour.ago)
    end

    it "debería tener los microposts correctos en el orden correcto" do
      expect(@usuario.microposts.to_a).to eq [micropost_mas_nuevo, micropost_antiguo]
    end

    it "debería eliminar los microposts asociados" do
      microposts = @usuario.microposts.to_a
      @usuario.destroy
      expect(microposts).not_to be_empty
      microposts.each do |micropost|
        expect(Micropost.where(id: micropost.id)).to be_empty
      end
    end

    describe "status" do
      let(:post_sin_seguidor) do
        FactoryGirl.create(:micropost, usuario: FactoryGirl.create(:usuario))
      end
      let(:usuario_seguido) { FactoryGirl.create(:usuario) }

      before do
        @usuario.seguir!(usuario_seguido)
        3.times { usuario_seguido.microposts.create!(content: "Lorem ipsum") }
      end

      its(:feed) { should include(micropost_mas_nuevo) }
      its(:feed) { should include(micropost_antiguo) }
      its(:feed) { should_not include(post_sin_seguidor) }
      its(:feed) do
        usuario_seguido.microposts.each do |micropost|
          should include(micropost)
        end
      end
    end
  end

  describe "siguiendo" do
    let(:otro_usuario) { FactoryGirl.create(:usuario) }
    before do
      @usuario.save
      @usuario.seguir!(otro_usuario)
    end

    it { should be_siguiendo(otro_usuario) }
    its(:usuarios_seguidos) { should include(otro_usuario) }

    describe "usuario seguido" do
      subject { otro_usuario }
      its(:seguidores) { should include(@usuario) }
    end

    describe "y no seguir" do
      before { @usuario.noseguir!(otro_usuario) }

      it { should_not be_siguiendo(otro_usuario) }
      its(:usuarios_seguidos) { should_not include(otro_usuario) }
    end
  end
end
