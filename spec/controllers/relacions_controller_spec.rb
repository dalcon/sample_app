require 'spec_helper'

describe RelacionsController do

  let(:usuario) { FactoryGirl.create(:usuario) }
  let(:otro_usuario) { FactoryGirl.create(:usuario) }

  before { ingresar usuario, no_capybara: true }

  describe "creando una relación con Ajax" do

    it "debería incrementarse el contador de Relacion" do
      expect do
        xhr :post, :create, relacion: { seguido_id: otro_usuario.id }
      end.to change(Relacion, :count).by(1)
    end

    it "debería responder con exito" do
      xhr :post, :create, relacion: { seguido_id: otro_usuario.id }
      expect(response).to be_success
    end
  end

  describe "eliminando una relación con Ajax" do

    before { usuario.seguir!(otro_usuario) }
    let(:relacion) do
      usuario.relacions.find_by(seguido_id: otro_usuario.id)
    end

    it "debería decrememtar el contador de Relacion" do
      expect do
        xhr :delete, :destroy, id: relacion.id
      end.to change(Relacion, :count).by(-1)
    end

    it "debería responder con exito" do
      xhr :delete, :destroy, id: relacion.id
      expect(response).to be_success
    end
  end
end
