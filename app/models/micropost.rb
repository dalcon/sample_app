class Micropost < ActiveRecord::Base
  belongs_to :usuario
  default_scope -> { order('created_at DESC') }
  validates :content, presence: true, length: { maximum: 140 }
  validates :usuario_id, presence: true

  def self.de_usuarios_seguidos_por(usuario)
    # el metodo de usuario usuarios_seguido_ids es dinamico utiliza el has_many
    # usuarios_seguidos sin la letra s al final y agregando ids para obtener
    # solo los ids de los objetos usuario.
    usuario_seguido_ids = "SELECT seguido_id FROM relacions
                           WHERE seguidor_id = :usuario_id"

    where("usuario_id IN (#{usuario_seguido_ids}) OR usuario_id = :usuario_id",
          usuario_id: usuario.id)
  end
end
