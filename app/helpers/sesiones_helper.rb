module SesionesHelper

  def ingresar(usuario)
    remember_token = Usuario.nuevo_remember_token
    cookies.permanent[:remember_token] = remember_token
    usuario.update_attribute(:remember_token, Usuario.digest(remember_token))
    self.usuario_actual = usuario
  end

  def ingresar?
    !usuario_actual.nil?
  end

  def usuario_actual=(usuario)
    @usuario_actual = usuario
  end

  def usuario_actual
    remember_token = Usuario.digest(cookies[:remember_token])
    @usuario_actual ||= Usuario.find_by(remember_token: remember_token)
  end

  def usuario_actual?(usuario)
    usuario == usuario_actual
  end

  def usuario_autenticado
    unless ingresar?
      guardar_localizacion
      redirect_to ingresar_url, notice: "Por favor ingrese."
    end
  end

  def salir
    usuario_actual.update_attribute(:remember_token,
                                    Usuario.digest(Usuario.nuevo_remember_token))
    cookies.delete(:remember_token)
    self.usuario_actual = nil
  end

  def regresar_o(default)
    redirect_to( session[:regresar_a] || default )
    session.delete(:regresar_a)
  end

  def guardar_localizacion
    session[:regresar_a] = request.url if request.get?
  end
end
