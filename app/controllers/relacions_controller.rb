class RelacionsController < ApplicationController
  before_action :usuario_autenticado

  def create
    @usuario = Usuario.find(params[:relacion][:seguido_id])
    usuario_actual.seguir!(@usuario)
    respond_to do |formato|
      formato.html { redirect_to @usuario }
      formato.js
    end
  end

  def destroy
    @usuario = Relacion.find(params[:id]).seguido
    usuario_actual.noseguir!(@usuario)
    respond_to do |formato|
      formato.html { redirect_to @usuario }
      formato.js
    end
  end
end
