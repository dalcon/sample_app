class SesionesController < ApplicationController
  def new
  end

  def create
    usuario = Usuario.find_by(email: params[:sesion][:email].downcase)
    if usuario && usuario.authenticate(params[:sesion][:password])
      ingresar usuario
      regresar_o usuario
    else
      flash.now[:error] = "Combinación de email/contraseña inválida"
      render 'new'
    end
  end

  def destroy
    salir
    redirect_to root_url
  end
end
