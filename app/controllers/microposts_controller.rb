class MicropostsController < ApplicationController
  before_action :usuario_autenticado, only: [:create, :destroy]
  before_action :usuario_correcto, only: :destroy

  def create
    @micropost = usuario_actual.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Micropost creado!"
      redirect_to root_url
    else
      @feed_items = []
      render 'paginas_estaticas/home'
    end
  end

  def destroy
    @micropost.destroy
    redirect_to root_url
  end

  private

    def micropost_params
      params.require(:micropost).permit(:content)
    end

    def usuario_correcto
      @micropost = usuario_actual.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
