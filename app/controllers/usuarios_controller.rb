class UsuariosController < ApplicationController
  before_action :usuario_identificado, only: [:index, :edit, :update, :destroy,
                                              :siguiendo, :seguidores]
  before_action :usuario_correcto,     only: [:edit, :update]
  before_action :usuario_admin,        only: :destroy

  def index
    @usuarios = Usuario.paginate(page: params[:page])
  end

  def show
    @usuario = Usuario.find(params[:id])
    @microposts = @usuario.microposts.paginate(page: params[:page])
  end

  def new
    @usuario = Usuario.new
  end

  def create
    @usuario = Usuario.new(usuario_params)
    if @usuario.save
      ingresar @usuario
      flash[:success] = "Bienvenido a la App sample!"
      redirect_to @usuario
    else
      render 'new'
    end
  end

  def edit
    #@usuario = Usuario.find(params[:id])
  end

  def update
    #@usuario = Usuario.find(params[:id])
    if @usuario.update_attributes(usuario_params)
      flash[:success] = "Perfil actualizado"
      redirect_to @usuario
    else
      render 'edit'
    end
  end

  def destroy
    Usuario.find(params[:id]).destroy
    flash[:success] = "Usuario eliminado."
    redirect_to usuarios_url
  end

  def siguiendo
    @title = "Siguiendo"
    @usuario = Usuario.find(params[:id])
    @usuarios = @usuario.usuarios_seguidos.paginate(page: params[:page])
    render 'show_seguir'
  end

  def seguidores
    @title = "Seguidores"
    @usuario = Usuario.find(params[:id])
    @usuarios = @usuario.seguidores.paginate(page: params[:page])
    render 'show_seguir'
  end

  private

    def usuario_params
      params.require(:usuario).permit(:nombre, :email,
                                      :password, :password_confirmation)
    end

    # Filtros Before.

    def usuario_identificado
      unless ingresar?
        guardar_localizacion
        redirect_to ingresar_url, notice: "Por favor identifiquese."
      end
    end

    def usuario_correcto
      @usuario = Usuario.find(params[:id])
      redirect_to(root_url) unless usuario_actual?(@usuario)
    end

    def usuario_admin
      redirect_to(root_url) unless usuario_actual.admin?
    end
end
