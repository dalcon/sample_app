class PaginasEstaticasController < ApplicationController
  def home
    if ingresar?
      @micropost = usuario_actual.microposts.build
      @feed_items = usuario_actual.feed.paginate(page: params[:page])
    end
  end

  def help
  end

  def about
  end

  def contacto
  end
end
