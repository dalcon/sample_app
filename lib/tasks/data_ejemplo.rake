namespace :db do
  desc "Llenar la base de datos con data de ejemplo"
  task populate: :environment do
    crear_usuarios
    crear_microposts
    crear_relaciones
  end
end

def crear_usuarios
  admin = Usuario.create!(nombre: "Usuario Ejemplo",
                          email: "example@railstutorial.org",
                          password: "foobar",
                          password_confirmation: "foobar",
                          admin: true )

  99.times do |n|
    nombre = Faker::Name.name
    email = "example-#{n+1}@railstutorial.org"
    password = "password"
    Usuario.create!(nombre: nombre,
                    email: email,
                    password: password,
                    password_confirmation: password)
  end
end

def crear_microposts
  usuarios = Usuario.all(limit: 6)

  50.times do
    contenido = Faker::Lorem.sentence(5)
    usuarios.each { |usuario| usuario.microposts.create!(content: contenido) }
  end
end

def crear_relaciones
  usuarios = Usuario.all
  usuario = usuarios.first
  usuarios_seguidos = usuarios[2..50]
  seguidores        = usuarios[3..40]
  usuarios_seguidos.each { |seguido| usuario.seguir!(seguido) }
  seguidores.each        { |seguidor| seguidor.seguir!(usuario) }
end
